import os
from RunRapidSim import gen_and_sel
from Selections import get_eff

def get_eff_factor(ngen=int(1e5), useKmumuPaperEff=False):
    decay = 'B2JpsiK'
    print(f'generating {decay}')
    os.system(f'RapidSim.exe decay_cfg/{decay} {ngen} 1 >> ./logfiles/{decay}_gen.log')
    mjpsi, taujpsi = 3.1, 0

    effjpsi = get_eff(decay, ngen, efftag=decay)

    L_7, L_8 = 1e15, 2e15 # 1/barn
    xs4pi_7, xs4pi_8, xs4pi_14 = 0.25e-3, 0.29e-3, 0.53e-3 #barn 
    fu, fd, fs, fc, fLb = 0.4, 0.4, 0.1, 0.002, 0.08
    NBu = (L_7*xs4pi_7 + L_8*xs4pi_8)*fu*2 #factor 2 because two B per event
    BRjpsi = 1.01e-3 * 5.96e-2

    yieldJpsi = NBu * BRjpsi * effjpsi.nominal_value
    if useKmumuPaperEff:
        yieldJpsi_paper = 980e3
        print('Normalising to yield of B->JpsiK obtained in arXiv:1612.07818')
    else:
        yieldJpsi_paper = 283e3
        print('Normalising to yield of B->JpsiK obtained in arXiv:1401.5361')

    eff_factor = yieldJpsi_paper / yieldJpsi
    print('eff factor = Njpsi_paper / Njpsi = {}'.format(eff_factor))

    conffile = os.environ['RAPIDSIM_ROOT'] + '/config/particles.dat'
    os.system(f'rm {decay}_hists.root')
    os.system(f'rm ./logfiles/{decay}_gen.log')
    os.system(f'rm {decay}_tree.root')


    return eff_factor
