L_7, L_8, L_13, L_run3 = 1e15, 2e15, 6e15, 15e15 # 1/barn (run1)
xs4pi_7, xs4pi_8, xs4pi_14 = 0.25e-3, 0.29e-3, 0.53e-3 #barn 
totbb = {'Run1': L_7*xs4pi_7 + L_8*xs4pi_8, 'Run2': L_13*xs4pi_14, 'Run3': L_run3*xs4pi_14}

fragfrac = {521: 0.38, 511: 0.38}
fragfrac[531] = 0.122 * (fragfrac[521] + fragfrac[511]) # arXiv:1902.06794
fragfrac[541] = 3.7e-3 * (fragfrac[521] + fragfrac[511]) # arXiv:1910.13404


