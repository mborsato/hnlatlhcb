import os

def gen_and_sel(decay, m, tau, ngen, efftag='Inclusive', verbose=False, keeptuple=False):
    c = 300. # µm/ps
    ID, part, anti = 9999, 'chi', '---'
    mass, width, charge = m, 0., 0.
    spin, shape, ctau = 0., '---', c*tau

    # add particle definition to rapidsim configuration file
    partdef = '{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}'.format(ID, part, anti, mass, width, charge, spin, shape, ctau)
    conffile = os.environ['RAPIDSIM_ROOT'] + '/config/particles.dat'
    os.system('echo "{}" >> {}'.format(partdef, conffile))

    # generate
    logfile = '{}_gen.log'.format(decay)
    if verbose: print('RapidSim generating {} events from {} with m={}, tau={}'.format(ngen, logfile, m, tau))
    os.system('RapidSim.exe ./decay_cfg/{} {} 1 > ./logfiles/{}'.format(decay, ngen, logfile))

    from Selections import get_eff
    eff = get_eff(decay, ngen, efftag)

    # get eff from logfile
    #f = open('B2Kchi_gen.log', 'r')
    #x=f.readlines()
    #nsel = [int(s) for s in x[-3].split() if s.isdigit()][0]
    #print('Efficiency is {}%'.format(nsel/ngen*100.))

    # clean up
    os.system("sed -i '' -e '$ d' {}".format(conffile))
    if os.path.exists('{}_hists.root'.format(decay)):
        os.system('rm {}_hists.root'.format(decay))
        os.system('rm ./logfiles/{}_gen.log'.format(decay))
        if not keeptuple:
            os.system('rm {}_tree.root'.format(decay))

    return eff




def gen_and_cut(decay, m, tau, ngen, efftag='Inclusive', verbose=False):
    c = 300. # µm/ps
    ID, part, anti = 9999, 'chi', '---'
    mass, width, charge = m, 0., 0.
    spin, shape, ctau = 0., '---', c*tau

    # add particle definition to rapidsim configuration file
    partdef = '{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}\t\t{}'.format(ID, part, anti, mass, width, charge, spin, shape, ctau)
    conffile = os.environ['RAPIDSIM_ROOT'] + '/config/particles.dat'
    os.system('echo "{}" >> {}'.format(partdef, conffile))

    # generate
    logfile = '{}_gen.log'.format(decay)
    if verbose: print('RapidSim generating {} events from {} with m={}, tau={}'.format(ngen, logfile, m, tau))
    os.system('RapidSim.exe ./decay_cfg/{} {} 1 > ./logfiles/{}'.format(decay, ngen, logfile))

    from Selections import cuts
    import root_pandas as rp
    d = rp.read_root(f'{decay}_tree.root', where=cuts[efftag])

    # clean up
    os.system("sed -i '' -e '$ d' {}".format(conffile))
    if os.path.exists('{}_hists.root'.format(decay)):
        os.system('rm {}_hists.root'.format(decay))
        os.system('rm ./logfiles/{}_gen.log'.format(decay))

    return d

