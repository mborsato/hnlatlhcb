import ROOT as r
from math import sqrt
from uncertainties import ufloat
from os import path

L0 = "(sqrt(mup_1_PT*mup_0_PT)>1.69 || max(mup_1_PT, mup_0_PT)>1.64)" # L0Muon or L0DiMuon according to paper
acc = "min(min(pim_0_eta, mup_0_eta), mup_1_eta)>2 && max(max(pim_0_eta, mup_0_eta), mup_1_eta)<4.5"
kin = "min(mup_0_P, mup_1_P)>5 && min(mup_0_PT, mup_1_PT)>0.75 && pim_0_P>3 && pim_0_PT>0.5 && mup_1_PT+pim_0_PT>0.7" # actual offline selection requires p_mu>3 and p_pi>2, but probably better to increase it to be in the region of maximum acceptance
IPchi2 = "min(min((mup_0_MINIP/mup_0_SIGMAMINIP_TRUE)**2, (mup_1_MINIP/mup_1_SIGMAMINIP_TRUE)**2), (pim_0_MINIP/pim_0_SIGMAMINIP_TRUE)**2)>50." # all MINIP chi2 larger than 50 as in offline selection
velo_vtx = 'chi_0_vtxZ<175e3 && sqrt(chi_0_vtxX**2 + chi_0_vtxY**2)<7e3'
#downstream_vtx = 'chi_0_vtxZ<1500e3 && sqrt(chi_0_vtxX**2 + chi_0_vtxY**2)<20e3'#FIXME assuming much better acceptance with downstream
downstream_vtx = 'chi_0_vtxZ<230e3 && sqrt(chi_0_vtxX**2 + chi_0_vtxY**2)<20e3'#FIXME assuming much better acceptance with downstream
chiPT = 'chi_0_PT>1.0'
mupiPT = 'mup_1_pim_0_M>1.0'

DP_all = 'max(mup_1_PT, mum_0_PT)>0.5 && min(mup_1_P, mum_0_P)>10 && max(mup_1_P, mum_0_P)<1000 && min(mup_1_eta, mum_0_eta)>2 && max(mup_1_eta, mum_0_eta)<4.5 && sqrt(mup_1_PT*mum_0_PT)>1'
DP_displ = 'chi_0_PT>2 && chi_0_PT<10 && chi_0_eta>2 && chi_0_eta<4.5 && sqrt(chi_0_vtxX**2 + chi_0_vtxY**2)>12e3 && sqrt(chi_0_vtxX**2 + chi_0_vtxY**2)<30e3'

e = ' && '
cuts = {
        'Inclusive_VELO': L0 +e+ acc +e+ kin +e+ IPchi2 +e+ velo_vtx,
        'Inclusive': L0 +e+ acc +e+ kin +e+ IPchi2 +e+ downstream_vtx +e+ chiPT,
        'Inclusive_noL0': acc +e+ kin +e+ IPchi2 +e+ downstream_vtx,
        'B2JpsiK': L0 +e+ acc +e+ kin +e+ IPchi2,
        'Inclusive_3mu': (L0 +e+ acc +e+ kin +e+ IPchi2 +e+ downstream_vtx +e+ mupiPT).replace('pim', 'mum'), 
        'Inclusive_mumue': (L0 +e+ acc +e+ kin +e+ IPchi2 +e+ downstream_vtx +e+ mupiPT).replace('pim', 'em'), 
        'DarkPhoton_mumunu': (DP_all +e+ DP_displ), 
        'NOselection' : '(1)',
        }

def get_eff(decay, ngen, efftag):
    tfilename = '{}_tree.root'.format(decay)
    if path.exists(tfilename):
        f = r.TFile(tfilename)
        t = f.Get('DecayTree')

        nsel = t.GetEntries(cuts[efftag])
        eff_val = nsel/ngen
        if nsel > 0:
            eff_err = eff_val/sqrt(nsel)
        else:
            eff_err = 3/ngen #FIXME need to get proper estimate for eff err in these cases?
        eff = ufloat(eff_val, eff_err)
    else:
        eff = ufloat(0, 0)

    return eff
