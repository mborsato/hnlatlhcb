import yaml
import numpy as np
import os

### LOAD CFG FILE ###
c = yaml.load(open('./config_incldecay.yml', 'r'), Loader=yaml.FullLoader)

def antipart(loki):
	s=loki;
	if (loki.count('+') != 0): s=loki.replace('+','-');
	elif(loki.count('-') != 0): s=loki.replace('-','+');
	s=s.replace('0','0bar')
	#print("final s", s)
	return s
	
def IDtoLoki(ID):
	#should read RAPIDSIM_ROOT+'/config/particles.dat', delimiter="\t" , usecols=columns_to_keep)
	fname = os.environ['RAPIDSIM_ROOT']+'/config/particles.dat'
	
	table=[]; flag=0;
	#lineList = list()
	#read file with one or several tab...
	with open(fname) as f:
		idx=0;
		for line in f:
			line=line.split('\t')
			#print("line", type(line))
			for i in line:
				if(i=='\t'): flag=1
			if (flag==1): line.remove('\t')
			#line=line.split('')
			flag=0;
			table.append(line);
			#print("line", type(line[0]))
			if(line[0]==str(ID)): return line[2]; 
			elif(line[0]==str(-1*ID)): return antipart(line[2]); 
			elif(ID==0):
				return ''
			#else: return 'xx'
			
	
def IDtofname(ID):
	s = IDtoLoki(ID)
	#print("IDtofname", s)
	s=s.replace('+', 'p');
	s=s.replace('-', 'm');
	s=s.replace('*', 'star');
	return s;
	

def decayloki(BID, hID, lID):
	Bloki = IDtoLoki(BID);
	#print("Bloki",Bloki); print("Bloki type",type(Bloki))
	Hloki = IDtoLoki(hID);
	#print("Hloki",Hloki); print("Hloki type",type(Hloki))
	Lloki = IDtoLoki(lID);
	#print("Lloki",Lloki); print("Lloki type",type(Lloki))
	
	#print(Bloki+' -> '+Hloki+' '+Lloki + ' { chi -> mu+ pi- }')
	
	return f'{Bloki} -> {{ chi -> mu+ pi- }} {Lloki} {Hloki}'
	
def decayfilename(BID, hID, lID):
	Bname = IDtofname(BID); #print ("Bname", Bname, type(Bname))
	Hname = IDtofname(hID); #print ("Hname", Hname, type(Hname))
	Lname = IDtofname(lID); #print ("Lname", Lname, type(Lname))
	
	return Bname+'2N'+Lname+Hname


BR_vec = [];


for pc in c['production_channels']:
    BID, hID, lID = pc[0], pc[1], pc[2]

    #write decay
    fname  = decayfilename(BID, hID, lID);
    f = open(f'./decay_cfg/{fname}.decay', "w")
    f.write(decayloki(BID, hID, lID))
    f.close()

    #print(BID, hID, lID, fname)
    
    #write config
    f2 = open(f"./decay_cfg/{fname}.config", "w")
    f2.write(f"energy : {c['energy']}\n")
    f2.write(f"geometry : {c['geometry']}\n")
    f2.write(f"Acceptance : {c['Acceptance']}\n")
    f2.write(f"paramsDecaying : {c['paramsDecaying']}\n")
    f2.write(f"paramsStable : {c['paramsStable']}\n")
    f2.write('@0\n\tname : B\n')
    f2.write('@1\n\tname : chi_0\n')
    f2.write('@2\n\tname : mup_0\n')
    f2.write(f"\t smear : {c['smear']}\n")
    f2.write(f"\t smear : {c['smearIP']}\n")
    if hID==0:
        #print('exclusive')
        f2.write('@3\n\tname : mup_1\n')
        f2.write(f"\t smear : {c['smear']}\n")
        f2.write(f"\t smear : {c['smearIP']}\n")
        f2.write('@4\n\tname : pim_0\n')
        f2.write(f"\t smear : {c['smear']}\n")
        f2.write(f"\t smear : {c['smearIP']}\n")
    else:
        #print('inclusive')
        f2.write('@3\n\tname : XXX\n')
        f2.write('@4\n\tname : mup_1\n')
        f2.write(f"\t smear : {c['smear']}\n")
        f2.write(f"\t smear : {c['smearIP']}\n")
        f2.write('@5\n\tname : pim_0\n')
        f2.write(f"\t smear : {c['smear']}\n")
        f2.write(f"\t smear : {c['smearIP']}\n")
    f2.close()

    
